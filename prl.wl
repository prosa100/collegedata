(* ::Package:: *)

ReverseRule[rhs_,lhs_]:=Rule[lhs,rhs];
MakeDataset[header_,data_]:=Dataset@Inner[ReverseRule,data,header,Association];
NullQ[Null]:=True;NullQ[___]:=False;
MissingQ=MatchQ[Missing];
NotMissingQ=Not[MissingQ@#]&;
