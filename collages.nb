(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     29034,        701]
NotebookOptionsPosition[     28521,        679]
NotebookOutlinePosition[     28863,        694]
CellTagsIndexPosition[     28820,        691]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"TimesHigherEducationWorldReputationRankings", "=", 
   RowBox[{"Association", "@@@", 
    RowBox[{
    "Import", "[", 
     "\"\<C:\\\\Users\\\\Paul\\\\Source\\\\Repos\\\\pl\\\\UniversityMaster\\\\\
Data\\\\TimesHigherEducationWorldReputationRankings2014.json\>\"", "]"}]}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"TimesHigherEducationWorldRankings", "=", 
   RowBox[{"Association", "@@@", 
    RowBox[{
    "Import", "[", 
     "\"\<C:\\\\Users\\\\Paul\\\\Source\\\\Repos\\\\pl\\\\UniversityMaster\\\\\
Data\\\\TimesHigherEducationWorldRankings2014.json\>\"", "]"}]}]}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.61563825379503*^9, 3.6156382578272505`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"Dataset", "@", "List"}], "@@", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{
     RowBox[{"Dataset", "[", 
      RowBox[{"JoinAcross", "[", 
       RowBox[{
       "TimesHigherEducationWorldReputationRankings", ",", 
        "TimesHigherEducationWorldRankings", ",", "\"\<Name\>\""}], "]"}], 
      "]"}], "[", 
     RowBox[{"Select", "[", 
      RowBox[{
       RowBox[{"#Region", "\[Equal]", "\"\<North America\>\""}], "&"}], "]"}],
      "]"}], "[", 
    RowBox[{"All", ",", 
     RowBox[{"{", 
      RowBox[{
      "\"\<Name\>\"", ",", "\"\<ReputationResearch\>\"", ",", 
       "\"\<OverallReputation\>\"", ",", "\"\<ReputationTeaching\>\"", ",", 
       "\"\<Teaching\>\"", ",", "\"\<IndustryIncome\>\"", ",", 
       "\"\<Citations\>\"", ",", "\"\<Overall\>\"", ",", 
       "\"\<InternationalOutlook\>\""}], "}"}]}], "]"}], 
   ")"}]}], "\[IndentingNewLine]", 
 RowBox[{"Normal", "@", 
  RowBox[{"%", "[", 
   RowBox[{"All", ",", "\"\<Name\>\""}], "]"}]}]}], "Input",
 CellChangeTimes->{{3.6156380586193*^9, 3.6156381338666553`*^9}, {
  3.6156382219141855`*^9, 3.6156382245293355`*^9}, {3.6156383099722853`*^9, 
  3.615638462104114*^9}, {3.615644158852804*^9, 3.6156441672452908`*^9}}],

Cell[BoxData[
 InterpretationBox[
  TagBox[
   StyleBox[
    PaneSelectorBox[{False->
     StyleBox[GridBox[{
        {
         RowBox[{"{", "\[ThinSpace]", 
          StyleBox[
           RowBox[{
            StyleBox["\[LeftAssociation]",
             LineColor->GrayLevel[0.45],
             FrontFaceColor->GrayLevel[0.45],
             BackFaceColor->GrayLevel[0.45],
             GraphicsColor->GrayLevel[0.45],
             FontColor->GrayLevel[0.45]], "\[ThinSpace]", 
            RowBox[{
             StyleBox["\<\"Name\"\>",
              LineColor->GrayLevel[0.3],
              FrontFaceColor->GrayLevel[0.3],
              BackFaceColor->GrayLevel[0.3],
              GraphicsColor->GrayLevel[0.3],
              FontColor->GrayLevel[0.4]], 
             StyleBox["\[Rule]",
              LineColor->GrayLevel[0.5],
              FrontFaceColor->GrayLevel[0.5],
              BackFaceColor->GrayLevel[0.5],
              GraphicsColor->GrayLevel[0.5],
              
              FontColor->GrayLevel[
               0.5]], "\<\"California Institute of Technology \
(Caltech)\"\>"}], ",", 
            RowBox[{
             StyleBox["\<\"ReputationResearch\"\>",
              LineColor->GrayLevel[0.3],
              FrontFaceColor->GrayLevel[0.3],
              BackFaceColor->GrayLevel[0.3],
              GraphicsColor->GrayLevel[0.3],
              FontColor->GrayLevel[0.4]], 
             StyleBox["\[Rule]",
              LineColor->GrayLevel[0.5],
              FrontFaceColor->GrayLevel[0.5],
              BackFaceColor->GrayLevel[0.5],
              GraphicsColor->GrayLevel[0.5],
              FontColor->GrayLevel[0.5]], "30.8`"}], ",", 
            RowBox[{
             StyleBox["\<\"OverallReputation\"\>",
              LineColor->GrayLevel[0.3],
              FrontFaceColor->GrayLevel[0.3],
              BackFaceColor->GrayLevel[0.3],
              GraphicsColor->GrayLevel[0.3],
              FontColor->GrayLevel[0.4]], 
             StyleBox["\[Rule]",
              LineColor->GrayLevel[0.5],
              FrontFaceColor->GrayLevel[0.5],
              BackFaceColor->GrayLevel[0.5],
              GraphicsColor->GrayLevel[0.5],
              FontColor->GrayLevel[0.5]], "29.2`"}], ",", 
            RowBox[{
             StyleBox["\<\"ReputationTeaching\"\>",
              LineColor->GrayLevel[0.3],
              FrontFaceColor->GrayLevel[0.3],
              BackFaceColor->GrayLevel[0.3],
              GraphicsColor->GrayLevel[0.3],
              FontColor->GrayLevel[0.4]], 
             StyleBox["\[Rule]",
              LineColor->GrayLevel[0.5],
              FrontFaceColor->GrayLevel[0.5],
              BackFaceColor->GrayLevel[0.5],
              GraphicsColor->GrayLevel[0.5],
              FontColor->GrayLevel[0.5]], "25.9`"}], ",", 
            StyleBox[
             SubscriptBox["\[CenterEllipsis]", "5"],
             LineColor->GrayLevel[0.5],
             FrontFaceColor->GrayLevel[0.5],
             BackFaceColor->GrayLevel[0.5],
             GraphicsColor->GrayLevel[0.5],
             FontColor->GrayLevel[0.5]], "\[ThinSpace]", 
            StyleBox["\[RightAssociation]",
             LineColor->GrayLevel[0.45],
             FrontFaceColor->GrayLevel[0.45],
             BackFaceColor->GrayLevel[0.45],
             GraphicsColor->GrayLevel[0.45],
             FontColor->GrayLevel[0.45]]}],
           LineBreakWithin->Automatic], ",", 
          StyleBox[
           SubscriptBox["\[CenterEllipsis]", "28"],
           LineColor->GrayLevel[0.5],
           FrontFaceColor->GrayLevel[0.5],
           BackFaceColor->GrayLevel[0.5],
           GraphicsColor->GrayLevel[0.5],
           FontColor->GrayLevel[0.5]], "\[ThinSpace]", "}"}]},
        {
         ItemBox[
          RowBox[{"2", "  ", "levels", 
           AdjustmentBox[
            RowBox[{" ", 
             RowBox[{"|", " "}]}],
            BoxBaselineShift->-0.3], "261", "  ", "elements"}],
          Alignment->{Left, Center},
          Background->RGBColor[0.92, 0.92, 0.9],
          
          BaseStyle->{
           FontFamily -> "Helvetica", FontSize -> 10, FontColor -> 
            GrayLevel[0.4], Editable -> False, Selectable -> False},
          ItemSize->{Automatic, Automatic}]}
       },
       FrameStyle->RGBColor[0, 0, 0, 0.4],
       GridBoxAlignment->{"Columns" -> {Left}},
       GridBoxFrame->{"ColumnsIndexed" -> {{{1, -1}, {1, -1}} -> True}},
       GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
       GridBoxSpacings->{"Columns" -> {
           Offset[1.4], {
            Offset[0.5599999999999999]}, 
           Offset[0.7]}, "ColumnsIndexed" -> {}, "Rows" -> {
           Offset[0.8], {
            Offset[0.4]}, 
           Offset[0.]}, "RowsIndexed" -> {}}],
      LineBreakWithin->Automatic], True->GridBox[{
       {
        RowBox[{"{", 
         StyleBox[
          SubscriptBox["\[CenterEllipsis]", "29"],
          LineColor->GrayLevel[0.5],
          FrontFaceColor->GrayLevel[0.5],
          BackFaceColor->GrayLevel[0.5],
          GraphicsColor->GrayLevel[0.5],
          FontColor->GrayLevel[0.5]], "}"}]},
       {
        ItemBox[
         RowBox[{"2", "  ", "levels", 
          AdjustmentBox[
           RowBox[{" ", 
            RowBox[{"|", " "}]}],
           BoxBaselineShift->-0.3], "261", "  ", "elements"}],
         Alignment->{Left, Center},
         Background->RGBColor[0.92, 0.92, 0.9],
         BaseStyle->{
          FontFamily -> "Helvetica", FontSize -> 10, FontColor -> 
           GrayLevel[0.4], Editable -> False, Selectable -> False},
         ItemSize->{Automatic, Automatic}]}
      },
      FrameStyle->RGBColor[0, 0, 0, 0.4],
      GridBoxAlignment->{"Columns" -> {Left}},
      GridBoxFrame->{"ColumnsIndexed" -> {{{1, -1}, {1, -1}} -> True}},
      GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
      GridBoxSpacings->{"Columns" -> {
          Offset[1.4], {
           Offset[0.5599999999999999]}, 
          Offset[0.7]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.8], {
           Offset[0.4]}, 
          Offset[0.]}, "RowsIndexed" -> {}}]}, Dynamic[
      CurrentValue[Evaluatable]],
     BaselinePosition->Baseline,
     ImageSize->Automatic],
    Editable->False,
    Selectable->True,
    ShowAutoStyles->False,
    LineSpacing->{1.1, 5},
    NumberMarks->False,
    SpanMinSize->1.,
    SpanMaxSize->3,
    LineBreakWithin->False,
    LineIndent->0,
    LinebreakAdjustments->{0.4, 5, 100, 0, 0.5},
    FontFamily->"Helvetica Neue",
    FontWeight->"Light",
    Background->RGBColor[0.985, 0.98, 0.973]],
   Deploy],
  Dataset[{
    Association[
    "Name" -> "California Institute of Technology (Caltech)", 
     "ReputationResearch" -> 30.8, "OverallReputation" -> 29.2, 
     "ReputationTeaching" -> 25.9, "Teaching" -> 94.4, "IndustryIncome" -> 
     91.2, "Citations" -> 99.8, "Overall" -> 94.9, "InternationalOutlook" -> 
     65.8], 
    Association[
    "Name" -> "Harvard University", "ReputationResearch" -> 100, 
     "OverallReputation" -> 100, "ReputationTeaching" -> 100, "Teaching" -> 
     95.3, "IndustryIncome" -> 40.6, "Citations" -> 99.1, "Overall" -> 93.9, 
     "InternationalOutlook" -> 66.2], 
    Association[
    "Name" -> "Stanford University", "ReputationResearch" -> 76.7, 
     "OverallReputation" -> 74.9, "ReputationTeaching" -> 71.3, "Teaching" -> 
     94.7, "IndustryIncome" -> 61.3, "Citations" -> 99.1, "Overall" -> 93.8, 
     "InternationalOutlook" -> 68], 
    Association[
    "Name" -> "Massachusetts Institute of Technology (MIT)", 
     "ReputationResearch" -> 92.8, "OverallReputation" -> 90.4, 
     "ReputationTeaching" -> 85.5, "Teaching" -> 92.9, "IndustryIncome" -> 
     94.3, "Citations" -> 100, "Overall" -> 93, "InternationalOutlook" -> 82], 
    Association[
    "Name" -> "Princeton University", "ReputationResearch" -> 36.5, 
     "OverallReputation" -> 35.7, "ReputationTeaching" -> 34, "Teaching" -> 
     89.9, "IndustryIncome" -> 80.5, "Citations" -> 99.7, "Overall" -> 92.7, 
     "InternationalOutlook" -> 59.6], 
    Association[
    "Name" -> "University of California, Berkeley", "ReputationResearch" -> 
     66.1, "OverallReputation" -> 63.1, "ReputationTeaching" -> 57.1, 
     "Teaching" -> 83.2, "IndustryIncome" -> 59.5, "Citations" -> 99.3, 
     "Overall" -> 89.8, "InternationalOutlook" -> 57.3], 
    Association[
    "Name" -> "University of Chicago", "ReputationResearch" -> 22.6, 
     "OverallReputation" -> 20.8, "ReputationTeaching" -> 17.2, "Teaching" -> 
     85.6, "IndustryIncome" -> 0, "Citations" -> 98, "Overall" -> 87.8, 
     "InternationalOutlook" -> 58.6], 
    Association[
    "Name" -> "Yale University", "ReputationResearch" -> 30.7, 
     "OverallReputation" -> 30.9, "ReputationTeaching" -> 31.2, "Teaching" -> 
     89.5, "IndustryIncome" -> 38.7, "Citations" -> 93.5, "Overall" -> 87.4, 
     "InternationalOutlook" -> 57.6], 
    Association[
    "Name" -> "University of California, Los Angeles (UCLA)", 
     "ReputationResearch" -> 29.9, "OverallReputation" -> 28.8, 
     "ReputationTeaching" -> 26.5, "Teaching" -> 84.8, "IndustryIncome" -> 0, 
     "Citations" -> 95.6, "Overall" -> 86.3, "InternationalOutlook" -> 46.4], 
    Association[
    "Name" -> "Columbia University", "ReputationResearch" -> 22.7, 
     "OverallReputation" -> 21.6, "ReputationTeaching" -> 19.5, "Teaching" -> 
     86.6, "IndustryIncome" -> 0, "Citations" -> 95.6, "Overall" -> 85.2, 
     "InternationalOutlook" -> 68], 
    Association[
    "Name" -> "Johns Hopkins University", "ReputationResearch" -> 17.7, 
     "OverallReputation" -> 16.8, "ReputationTeaching" -> 14.9, "Teaching" -> 
     75.7, "IndustryIncome" -> 100, "Citations" -> 95, "Overall" -> 83.7, 
     "InternationalOutlook" -> 59.3], 
    Association[
    "Name" -> "University of Pennsylvania", "ReputationResearch" -> 13.7, 
     "OverallReputation" -> 12.8, "ReputationTeaching" -> 11.2, "Teaching" -> 
     79.8, "IndustryIncome" -> 45.2, "Citations" -> 95, "Overall" -> 81, 
     "InternationalOutlook" -> 40.6], 
    Association[
    "Name" -> "Duke University", "ReputationResearch" -> 9.8, 
     "OverallReputation" -> 9.4, "ReputationTeaching" -> 8.5, "Teaching" -> 
     73.9, "IndustryIncome" -> 100, "Citations" -> 96.7, "Overall" -> 79.3, 
     "InternationalOutlook" -> 50], 
    Association[
    "Name" -> "University of Michigan", "ReputationResearch" -> 20.6, 
     "OverallReputation" -> 18.9, "ReputationTeaching" -> 15.6, "Teaching" -> 
     70, "IndustryIncome" -> 53.5, "Citations" -> 90.8, "Overall" -> 79.2, 
     "InternationalOutlook" -> 49.5], 
    Association[
    "Name" -> "Cornell University", "ReputationResearch" -> 16.9, 
     "OverallReputation" -> 16.9, "ReputationTeaching" -> 16.7, "Teaching" -> 
     72.1, "IndustryIncome" -> 35.8, "Citations" -> 90.8, "Overall" -> 79.1, 
     "InternationalOutlook" -> 55.6], 
    Association[
    "Name" -> "University of Toronto", "ReputationResearch" -> 15.1, 
     "OverallReputation" -> 14.9, "ReputationTeaching" -> 14.5, "Teaching" -> 
     73.6, "IndustryIncome" -> 45.8, "Citations" -> 84.5, "Overall" -> 78.3, 
     "InternationalOutlook" -> 70], 
    Association[
    "Name" -> "Northwestern University", "ReputationResearch" -> 8.8, 
     "OverallReputation" -> 8.3, "ReputationTeaching" -> 7.4, "Teaching" -> 
     70.2, "IndustryIncome" -> 61.9, "Citations" -> 97.1, "Overall" -> 77.1, 
     "InternationalOutlook" -> 34.4], 
    Association[
    "Name" -> "Carnegie Mellon University", "ReputationResearch" -> 10.1, 
     "OverallReputation" -> 9.6, "ReputationTeaching" -> 8.5, "Teaching" -> 
     63.1, "IndustryIncome" -> 52.5, "Citations" -> 93.9, "Overall" -> 76, 
     "InternationalOutlook" -> 58.1], 
    Association[
    "Name" -> "University of Washington", "ReputationResearch" -> 9.4, 
     "OverallReputation" -> 9, "ReputationTeaching" -> 8.3, "Teaching" -> 
     65.6, "IndustryIncome" -> 43.1, "Citations" -> 95.6, "Overall" -> 73.4, 
     "InternationalOutlook" -> 43.2], 
    Association[
    "Name" -> "University of Texas at Austin", "ReputationResearch" -> 9.1, 
     "OverallReputation" -> 8.6, "ReputationTeaching" -> 7.6, "Teaching" -> 
     64.7, "IndustryIncome" -> 59.6, "Citations" -> 91.3, "Overall" -> 72.2, 
     "InternationalOutlook" -> 42.4], 
    Association[
    "Name" -> "Georgia Institute of Technology (Georgia Tech)", 
     "ReputationResearch" -> 8.2, "OverallReputation" -> 7.6, 
     "ReputationTeaching" -> 6.4, "Teaching" -> 59.4, "IndustryIncome" -> 
     71.3, "Citations" -> 87.9, "Overall" -> 71.6, "InternationalOutlook" -> 
     67.8], 
    Association[
    "Name" -> "University of Illinois at Urbana Champaign", 
     "ReputationResearch" -> 13.5, "OverallReputation" -> 12.7, 
     "ReputationTeaching" -> 11.3, "Teaching" -> 66, "IndustryIncome" -> 0, 
     "Citations" -> 79.4, "Overall" -> 71.4, "InternationalOutlook" -> 41.1], 
    Association[
    "Name" -> "University of Wisconsin-Madison", "ReputationResearch" -> 10.2,
      "OverallReputation" -> 10.2, "ReputationTeaching" -> 10, "Teaching" -> 
     69.8, "IndustryIncome" -> 51.2, "Citations" -> 87.2, "Overall" -> 71.1, 
     "InternationalOutlook" -> 32.3], 
    Association[
    "Name" -> "University of British Columbia", "ReputationResearch" -> 8.7, 
     "OverallReputation" -> 8.6, "ReputationTeaching" -> 8.3, "Teaching" -> 
     59.9, "IndustryIncome" -> 43.1, "Citations" -> 83.7, "Overall" -> 70.8, 
     "InternationalOutlook" -> 84.2], 
    Association[
    "Name" -> "McGill University", "ReputationResearch" -> 8.6, 
     "OverallReputation" -> 8.6, "ReputationTeaching" -> 8.7, "Teaching" -> 
     61.5, "IndustryIncome" -> 40.8, "Citations" -> 74.5, "Overall" -> 68.1, 
     "InternationalOutlook" -> 77.8], 
    Association[
    "Name" -> "New York University (NYU)", "ReputationResearch" -> 11, 
     "OverallReputation" -> 10.3, "ReputationTeaching" -> 8.7, "Teaching" -> 
     65.4, "IndustryIncome" -> 29.9, "Citations" -> 87.9, "Overall" -> 67.4, 
     "InternationalOutlook" -> 41.8], 
    Association[
    "Name" -> "University of California, San Diego", "ReputationResearch" -> 
     7.9, "OverallReputation" -> 7.2, "ReputationTeaching" -> 5.6, "Teaching" -> 
     52, "IndustryIncome" -> 48.4, "Citations" -> 96.7, "Overall" -> 67.4, 
     "InternationalOutlook" -> 35.6], 
    Association[
    "Name" -> "Pennsylvania State University", "ReputationResearch" -> 7.6, 
     "OverallReputation" -> 7.5, "ReputationTeaching" -> 7.2, "Teaching" -> 
     55.3, "IndustryIncome" -> 60, "Citations" -> 79.4, "Overall" -> 64.2, 
     "InternationalOutlook" -> 33.7], 
    Association[
    "Name" -> "Purdue University", "ReputationResearch" -> 6, 
     "OverallReputation" -> 6, "ReputationTeaching" -> 6.2, "Teaching" -> 
     53.7, "IndustryIncome" -> 0, "Citations" -> 67, "Overall" -> 60.7, 
     "InternationalOutlook" -> 60.6]}, 
   TypeSystem`Vector[
    TypeSystem`Assoc[
     TypeSystem`Atom[String], 
     TypeSystem`Atom[TypeSystem`AnyType], 9], 29], 
   Association["Origin" -> HoldComplete[
      Apply[
       Dataset`DatasetHandle[148000296155842], 
       Dataset`DatasetHandle[211432668150466]]], "ID" -> 
    165098560961218]]]], "Output",
 CellChangeTimes->{{3.6156384092690444`*^9, 3.615638462626145*^9}, {
  3.615644168038347*^9, 3.615644178335951*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"\<\"California Institute of Technology (Caltech)\"\>", 
   ",", "\<\"Harvard University\"\>", ",", "\<\"Stanford University\"\>", 
   ",", "\<\"Massachusetts Institute of Technology (MIT)\"\>", 
   ",", "\<\"Princeton University\"\>", 
   ",", "\<\"University of California, Berkeley\"\>", 
   ",", "\<\"University of Chicago\"\>", ",", "\<\"Yale University\"\>", 
   ",", "\<\"University of California, Los Angeles (UCLA)\"\>", 
   ",", "\<\"Columbia University\"\>", 
   ",", "\<\"Johns Hopkins University\"\>", 
   ",", "\<\"University of Pennsylvania\"\>", ",", "\<\"Duke University\"\>", 
   ",", "\<\"University of Michigan\"\>", ",", "\<\"Cornell University\"\>", 
   ",", "\<\"University of Toronto\"\>", 
   ",", "\<\"Northwestern University\"\>", 
   ",", "\<\"Carnegie Mellon University\"\>", 
   ",", "\<\"University of Washington\"\>", 
   ",", "\<\"University of Texas at Austin\"\>", 
   ",", "\<\"Georgia Institute of Technology (Georgia Tech)\"\>", 
   ",", "\<\"University of Illinois at Urbana Champaign\"\>", 
   ",", "\<\"University of Wisconsin-Madison\"\>", 
   ",", "\<\"University of British Columbia\"\>", 
   ",", "\<\"McGill University\"\>", ",", "\<\"New York University (NYU)\"\>",
    ",", "\<\"University of California, San Diego\"\>", 
   ",", "\<\"Pennsylvania State University\"\>", 
   ",", "\<\"Purdue University\"\>"}], "}"}]], "Output",
 CellChangeTimes->{{3.6156384092690444`*^9, 3.615638462626145*^9}, {
  3.615644168038347*^9, 3.615644178339934*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"USNewsUndergradComputerRankUndergradData", "=", 
  "\[IndentingNewLine]", 
  RowBox[{"Dataset", "@", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"<|", 
      RowBox[{
       RowBox[{"\"\<University\>\"", "->", 
        TemplateBox[{
         "\"Rose-Hulman Institute of Technology\"",RowBox[{"Entity", "[", 
            
            RowBox[{"\"University\"", ",", 
              "\"RoseHulmanInstituteOfTechnology152318\""}], "]"}],
          "\"Entity[\\\"University\\\", \
\\\"RoseHulmanInstituteOfTechnology152318\\\"]\"","\"university\""},
         "Entity"]}], ",", 
       RowBox[{
       "\"\<USNews.Undergrad.ComputerRankUndergrad\>\"", "\[Rule]", "1"}]}], 
      "|>"}], ",", "\[IndentingNewLine]", 
     RowBox[{"<|", 
      RowBox[{
       RowBox[{"\"\<University\>\"", "->", 
        TemplateBox[{"\"Harvey Mudd College\"",RowBox[{"Entity", "[", 
            RowBox[{"\"University\"", ",", "\"HarveyMuddCollege115409\""}], 
            "]"}],"\"Entity[\\\"University\\\", \\\"HarveyMuddCollege115409\\\
\"]\"","\"university\""},
         "Entity"]}], ",", 
       RowBox[{
       "\"\<USNews.Undergrad.ComputerRankUndergrad\>\"", "\[Rule]", "2"}]}], 
      "|>"}], ",", "\[IndentingNewLine]", 
     RowBox[{"<|", 
      RowBox[{
       RowBox[{"\"\<University\>\"", "->", 
        TemplateBox[{
         "\"California Polytechnic State University-San Luis Obispo\"",
          RowBox[{"Entity", "[", 
            
            RowBox[{"\"University\"", ",", 
              "\"CaliforniaPolytechnicStateUniversitySanLuisObispo110422\""}],
             "]"}],"\"Entity[\\\"University\\\", \
\\\"CaliforniaPolytechnicStateUniversitySanLuisObispo110422\\\"]\"",
          "\"university\""},
         "Entity"]}], ",", 
       RowBox[{
       "\"\<USNews.Undergrad.ComputerRankUndergrad\>\"", "\[Rule]", "3"}]}], 
      "|>"}]}], "}"}]}]}]], "Input",
 CellChangeTimes->{{3.6156442470598974`*^9, 3.615644484392662*^9}}],

Cell[BoxData[
 InterpretationBox[
  TagBox[
   StyleBox[
    PaneSelectorBox[{False->GridBox[{
       {
        StyleBox["\<\"University\"\>",
         LineColor->GrayLevel[0.3],
         FrontFaceColor->GrayLevel[0.3],
         BackFaceColor->GrayLevel[0.3],
         GraphicsColor->GrayLevel[0.3],
         FontColor->GrayLevel[0.3]], 
        StyleBox["\<\"USNews.Undergrad.ComputerRankUndergrad\"\>",
         LineColor->GrayLevel[0.3],
         FrontFaceColor->GrayLevel[0.3],
         BackFaceColor->GrayLevel[0.3],
         GraphicsColor->GrayLevel[0.3],
         FontColor->GrayLevel[0.3]]},
       {
        InterpretationBox[
         FrameBox[
          StyleBox[
           RowBox[{"Rose", "-", 
            RowBox[{
            "Hulman", " ", "Institute", " ", "of", " ", "Technology"}]}],
           LineColor->GrayLevel[0.3],
           FrontFaceColor->GrayLevel[0.3],
           BackFaceColor->GrayLevel[0.3],
           GraphicsColor->GrayLevel[0.3],
           FontColor->GrayLevel[0.3]],
          Background->RGBColor[0.999527, 0.97705, 0.907225],
          BaselinePosition->Baseline,
          FrameMargins->{{2, 4}, {0, 0}},
          FrameStyle->RGBColor[{
             Rational[208, 255], 
             Rational[202, 255], 
             Rational[176, 255]}],
          RoundingRadius->3],
         Entity["University", "RoseHulmanInstituteOfTechnology152318"]], "1"},
       {
        InterpretationBox[
         FrameBox[
          StyleBox[
           RowBox[{"Harvey", " ", "Mudd", " ", "College"}],
           LineColor->GrayLevel[0.3],
           FrontFaceColor->GrayLevel[0.3],
           BackFaceColor->GrayLevel[0.3],
           GraphicsColor->GrayLevel[0.3],
           FontColor->GrayLevel[0.3]],
          Background->RGBColor[0.999527, 0.97705, 0.907225],
          BaselinePosition->Baseline,
          FrameMargins->{{2, 4}, {0, 0}},
          FrameStyle->RGBColor[{
             Rational[208, 255], 
             Rational[202, 255], 
             Rational[176, 255]}],
          RoundingRadius->3],
         Entity["University", "HarveyMuddCollege115409"]], "2"},
       {
        InterpretationBox[
         FrameBox[
          StyleBox[
           RowBox[{
            RowBox[{
            "California", " ", "Polytechnic", " ", "State", " ", 
             "University"}], "-", 
            RowBox[{"San", " ", "Luis", " ", "Obispo"}]}],
           LineColor->GrayLevel[0.3],
           FrontFaceColor->GrayLevel[0.3],
           BackFaceColor->GrayLevel[0.3],
           GraphicsColor->GrayLevel[0.3],
           FontColor->GrayLevel[0.3]],
          Background->RGBColor[0.999527, 0.97705, 0.907225],
          BaselinePosition->Baseline,
          FrameMargins->{{2, 4}, {0, 0}},
          FrameStyle->RGBColor[{
             Rational[208, 255], 
             Rational[202, 255], 
             Rational[176, 255]}],
          RoundingRadius->3],
         Entity[
         "University", 
          "CaliforniaPolytechnicStateUniversitySanLuisObispo110422"]], "3"},
       {
        ItemBox[
         RowBox[{"2", "  ", "levels", 
          AdjustmentBox[
           RowBox[{" ", 
            RowBox[{"|", " "}]}],
           BoxBaselineShift->-0.3], "6", "  ", "elements"}],
         Alignment->{Left, Center},
         Background->RGBColor[0.92, 0.92, 0.9],
         BaseStyle->{
          FontFamily -> "Helvetica", FontSize -> 10, FontColor -> 
           GrayLevel[0.4], Editable -> False, Selectable -> False},
         ItemSize->{Automatic, Automatic}], "\[SpanFromLeft]"}
      },
      FrameStyle->RGBColor[0, 0, 0, 0.4],
      GridBoxAlignment->{"Columns" -> {{Left}}},
      GridBoxBackground->{"Columns" -> {}, "Rows" -> {
          RGBColor[0.92, 0.92, 0.9]}},
      GridBoxDividers->{"Columns" -> {
          RGBColor[0, 0, 0, 0.4], {
           RGBColor[0, 0, 0, 0.09]}, 
          RGBColor[0, 0, 0, 0.4]}, "Rows" -> {
          RGBColor[0, 0, 0, 0.4], 
          RGBColor[0, 0, 0, 0.3], {
           RGBColor[0, 0, 0, 0.08]}, 
          RGBColor[0, 0, 0, 0.4]}},
      GridBoxFrame->{"ColumnsIndexed" -> {{{1, -1}, {1, -1}} -> True}},
      GridBoxItemSize->{
       "Columns" -> {{All}}, "ColumnsIndexed" -> {}, "Rows" -> {{1.3}}, 
        "RowsIndexed" -> {}},
      GridBoxSpacings->{"Columns" -> {
          Offset[1.4], {
           Offset[1.75]}, 
          Offset[4.199999999999999]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[1.2], 
          Offset[0.6], {
           Offset[0.48]}, 
          Offset[0.4]}, "RowsIndexed" -> {}}], True->GridBox[{
       {
        RowBox[{"{", 
         StyleBox[
          SubscriptBox["\[CenterEllipsis]", "3"],
          LineColor->GrayLevel[0.5],
          FrontFaceColor->GrayLevel[0.5],
          BackFaceColor->GrayLevel[0.5],
          GraphicsColor->GrayLevel[0.5],
          FontColor->GrayLevel[0.5]], "}"}]},
       {
        ItemBox[
         RowBox[{"2", "  ", "levels", 
          AdjustmentBox[
           RowBox[{" ", 
            RowBox[{"|", " "}]}],
           BoxBaselineShift->-0.3], "6", "  ", "elements"}],
         Alignment->{Left, Center},
         Background->RGBColor[0.92, 0.92, 0.9],
         BaseStyle->{
          FontFamily -> "Helvetica", FontSize -> 10, FontColor -> 
           GrayLevel[0.4], Editable -> False, Selectable -> False},
         ItemSize->{Automatic, Automatic}]}
      },
      FrameStyle->RGBColor[0, 0, 0, 0.4],
      GridBoxAlignment->{"Columns" -> {Left}},
      GridBoxFrame->{"ColumnsIndexed" -> {{{1, -1}, {1, -1}} -> True}},
      GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}},
      GridBoxSpacings->{"Columns" -> {
          Offset[1.4], {
           Offset[0.5599999999999999]}, 
          Offset[0.7]}, "ColumnsIndexed" -> {}, "Rows" -> {
          Offset[0.8], {
           Offset[0.4]}, 
          Offset[0.]}, "RowsIndexed" -> {}}]}, Dynamic[
      CurrentValue[Evaluatable]],
     BaselinePosition->Baseline,
     ImageSize->Automatic],
    Editable->False,
    Selectable->True,
    ShowAutoStyles->False,
    LineSpacing->{1.1, 5},
    NumberMarks->False,
    SpanMinSize->1.,
    SpanMaxSize->3,
    LineBreakWithin->False,
    LineIndent->0,
    LinebreakAdjustments->{0.4, 5, 100, 0, 0.5},
    FontFamily->"Helvetica Neue",
    FontWeight->"Light",
    Background->RGBColor[0.985, 0.98, 0.973]],
   Deploy],
  Dataset[{
    Association[
    "University" -> 
     Entity["University", "RoseHulmanInstituteOfTechnology152318"], 
     "USNews.Undergrad.ComputerRankUndergrad" -> 1], 
    Association[
    "University" -> Entity["University", "HarveyMuddCollege115409"], 
     "USNews.Undergrad.ComputerRankUndergrad" -> 2], 
    Association[
    "University" -> 
     Entity["University", 
       "CaliforniaPolytechnicStateUniversitySanLuisObispo110422"], 
     "USNews.Undergrad.ComputerRankUndergrad" -> 3]}, 
   TypeSystem`Vector[
    TypeSystem`Struct[{
     "University", "USNews.Undergrad.ComputerRankUndergrad"}, {
      TypeSystem`Atom[
       Entity["University"]], 
      TypeSystem`Atom[Integer]}], 3], 
   Association["ID" -> 219661825489909]]]], "Output",
 CellChangeTimes->{3.6156444848206778`*^9}]
}, Open  ]]
},
WindowSize->{1920, 997},
WindowMargins->{{-8, Automatic}, {Automatic, -8}},
FrontEndVersion->"10.0 for Microsoft Windows (64-bit) (July 1, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 712, 17, 52, "Input"],
Cell[CellGroupData[{
Cell[1295, 41, 1225, 30, 72, "Input"],
Cell[2523, 73, 15338, 328, 56, "Output"],
Cell[17864, 403, 1532, 27, 92, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[19433, 435, 1948, 47, 130, "Input"],
Cell[21384, 484, 7121, 192, 144, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
